#!/bin/bash

export OTB_MALTA_ROOT="$( cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P )"
if [ ! -d $OTB_MALTA_ROOT/install/venv ]
then
    python3 -m venv $OTB_MALTA_ROOT/install/venv
    source $OTB_MALTA_ROOT/install/venv/bin/activate
    pip install -r requirements.txt
else
    source $OTB_MALTA_ROOT/install/venv/bin/activate
fi
cd $OTB_MALTA_ROOT/install/OTB-7.2.0-Linux64/
source ./otbenv.profile
# export DHUS_USER=
# export DHUS_PASSWORD=
cd $OTB_MALTA_ROOT
