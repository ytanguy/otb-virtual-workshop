#include "otbImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbSimpleRcsPanSharpeningFusionImageFilter.h"

int main(int argc, char * argv[])
{
  using InputPixelType       = double;
  using InputImageType       = otb::Image<InputPixelType, 2>;
  using InputVectorImageType = otb::VectorImage<InputPixelType, 2>;
  
  using ReaderType              = otb::ImageFileReader<InputImageType>;
  ReaderType::Pointer readerPAN = ReaderType::New();
  readerPAN->SetFileName(argv[1]);

  using ReaderVectorType             = otb::ImageFileReader<InputVectorImageType>;
  ReaderVectorType::Pointer readerXS = ReaderVectorType::New();
  readerXS->SetFileName(argv[2]);

  using OutputPixelType       = unsigned short int;
  using OutputVectorImageType = otb::VectorImage<OutputPixelType, 2>;
  
  using FilterType           = otb::SimpleRcsPanSharpeningFusionImageFilter
    <InputImageType, InputVectorImageType, OutputVectorImageType>;
  FilterType::Pointer filter = FilterType::New();
  filter->SetPanInput(readerPAN->GetOutput());
  filter->SetXsInput(readerXS->GetOutput());
  
  using WriterType           = otb::ImageFileWriter<OutputVectorImageType>;
  WriterType::Pointer writer = WriterType::New();

  writer->SetFileName(argv[3]);
  writer->SetInput(filter->GetOutput());

  writer->Update();
}
