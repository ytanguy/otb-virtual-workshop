#!/usr/bin/python

# Import the otb applications package
import otbApplication as otb

# The following line creates an instance of the Pansharpening application
PanSharpening = otb.Registry.CreateApplication("Pansharpening")

# The following lines set all the application parameters:
PanSharpening.SetParameterString("inp", "QB_Toulouse_Ortho_PAN.tif")
PanSharpening.SetParameterString("inxs", "QB_Toulouse_Ortho_XS.tif")
PanSharpening.SetParameterString("out", "QB_Toulouse_Ortho_PXS.tif")
PanSharpening.SetParameterOutputImagePixelType("out", otb.ImagePixelType_uint16)

# The following line execute the application
PanSharpening.ExecuteAndWriteOutput()
