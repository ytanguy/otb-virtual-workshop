#!/bin/bash
# usage: ./convert_img_for_notebook.sh img_to_convert width | xsel --clipboard
# $1: image to convert
# $2: image width
img_base64=$(base64 -w 0 $1)
img_width=$2
echo -n "<img src=\"data:image/png;base64,$img_base64\" width=${img_width}px>"
